var tinhTongTienDien = function (kw) {
  if (0 <= kw && kw <= 50) {
    return 500 * kw;
  } else if (51 <= kw && kw <= 100) {
    return 500 * 50 + 650 * (kw - 50);
  } else if (101 <= kw && kw <= 200) {
    return 500 * 50 + 650 * 50 + (kw - 100) * 850;
  } else if (201 <= kw && kw <= 350) {
    return 500 * 50 + 650 * 50 + 100 * 850 + (kw - 200) * 1100;
  } else if (kw >= 351) {
    return 500 * 50 + 650 * 50 + 100 * 850 + 150 * 1100 + (kw - 300) * 1300;
  }
};
var tinhTienDien = document
  .getElementById("btn-tien-dien")
  .addEventListener("click", function () {
    var khachHang = document.getElementById("chu-ho").value;
    var soKw = document.getElementById("so-Kw").value;
    console.log({ soKw });
    var tienDien = tinhTongTienDien(soKw);
    console.log(tienDien);
    document.getElementById(
      "ketQua"
    ).innerHTML = `<p>Khách hàng: ${khachHang}</p>
    <p>Tiền điện phải trả: ${tienDien} vnd</> `;
  });

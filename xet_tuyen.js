var tinhDiemCongkhuVuc = function (a) {
  switch (a) {
    case "A": {
      return 2;
    }
    case "B": {
      return 1;
    }
    case "C": {
      return 0.5;
    }
    case "X": {
      return 0;
    }
    default:
      alert("vui long nhap lai khu vuc");
  }
};
var tinhDiemCongDoiTuong = function (doiTuong) {
  switch (doiTuong) {
    case "1": {
      return 2.5;
    }
    case "2": {
      return 1.5;
    }
    case "3": {
      return 1;
    }
    case "0": {
      return 0;
    }
  }
};
var tinhTongDiem3Mon = function (monA, monB, monC, diemKhuVuc, diemDoiTuong) {
  var tongDiem = monA + monB + monC + diemDoiTuong + diemKhuVuc;
  return tongDiem;
};
var ketQuaXetTuyen = function (tongDiem, diemChuan, monA, monB, monC) {
  if (tongDiem >= diemChuan && monA > 0 && monB > 0 && monC > 0) {
    return "Thi đạt";
  } else {
    return "Ban đã thi trượt";
  }
};
document
  .getElementById("btn-kq-xet-tuyen")
  .addEventListener("click", function () {
    var diemChuan = document.getElementById("diem-chuan").value;
    console.log({ diemChuan });
    var monA = document.getElementById("diem-mon-A").value * 1;
    console.log({ monA });
    var monB = document.getElementById("diem-mon-B").value * 1;
    console.log({ monB });
    var monC = document.getElementById("diem-mon-C").value * 1;
    console.log({ monC });
    var khuVuc = document.querySelector(
      'input[name="selector"]:checked'
    )?.value;
    console.log({ khuVuc });
    var doiTuong = $("#doi-tuong").find(":selected").text();
    console.log({ doiTuong });
    console.log(tinhDiemCongkhuVuc(khuVuc));
    console.log(tinhDiemCongDoiTuong(doiTuong));
    var tongDiem3Mon = tinhTongDiem3Mon(
      monA,
      monB,
      monC,
      tinhDiemCongkhuVuc(khuVuc),
      tinhDiemCongDoiTuong(doiTuong)
    );
    var ketqua = ketQuaXetTuyen(tongDiem3Mon, diemChuan, monA, monB, monC);
    document.getElementById("ketQua").innerHTML = `<p>Kết Qủa: ${ketqua}</p>
    <p>Tổng điểm của bạn: ${tongDiem3Mon}`;
  });
